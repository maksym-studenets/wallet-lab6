package ua.edu.chnu.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.edu.chnu.wallet.model.OrderedFiles;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderedFilesRepository extends JpaRepository<OrderedFiles, Long> {

    List<OrderedFiles> findAllByTransactionId(final Long transactionId);

    Optional<OrderedFiles> findFirstByTransactionIdAndFileId(final Long transactionId, final Long fileId);
}
