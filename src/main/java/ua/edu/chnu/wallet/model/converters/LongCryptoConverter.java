package ua.edu.chnu.wallet.model.converters;

import ua.edu.chnu.wallet.model.converters.secure.EncryptionInitializer;

import javax.persistence.Converter;

@Converter
public class LongCryptoConverter extends AbstractCryptoConverter<Long> {

    public LongCryptoConverter() {
        this(new EncryptionInitializer());
    }

    public LongCryptoConverter(EncryptionInitializer initializer) {
        this.initializer = initializer;
    }

    @Override
    public String attributeToString(Long attribute) {
        return attribute.toString();
    }

    @Override
    public Long stringToAttribute(String string) {
        return Long.valueOf(string);
    }

    @Override
    public boolean isNotNullOrEmpty(Long attribute) {
        return attribute != null;
    }
}
