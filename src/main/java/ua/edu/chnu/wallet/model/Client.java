package ua.edu.chnu.wallet.model;

import lombok.Getter;
import lombok.Setter;
import ua.edu.chnu.wallet.model.base.Identifiable;
import ua.edu.chnu.wallet.model.converters.StringCryptoConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "clients")
public class Client implements Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int", nullable = false)
    private Long id;

    @Column(nullable = false)
    @Convert(converter = StringCryptoConverter.class)
    private String name;

    @Column(nullable = false)
    @Convert(converter = StringCryptoConverter.class)
    private String token;
}
