package ua.edu.chnu.wallet.model.converters.secure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EncryptionKey {

    public static String DATABASE_ENCRYPTION_KEY;
    public static String INIT_VECTOR;

    @Value("${encryption.key}")
    public void setDatabaseEncryptionKey(String databaseEncryptionKey) {
        DATABASE_ENCRYPTION_KEY = databaseEncryptionKey;
    }

    @Value("${encryption.initVector}")
    public void setInitVector(String initVector) {
        INIT_VECTOR = initVector;
    }
}
