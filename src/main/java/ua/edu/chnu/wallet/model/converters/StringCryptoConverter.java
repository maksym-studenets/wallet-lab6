package ua.edu.chnu.wallet.model.converters;

import ua.edu.chnu.wallet.model.converters.secure.EncryptionInitializer;

import javax.persistence.Converter;
import java.util.Base64;

@Converter
public class StringCryptoConverter extends AbstractCryptoConverter<String> {

    public StringCryptoConverter() {
        this(new EncryptionInitializer());
    }

    public StringCryptoConverter(EncryptionInitializer cipherInitializer) {
        super(cipherInitializer);
    }

    @Override
    public String attributeToString(String attribute) {
        return attribute;
    }

    @Override
    public String stringToAttribute(String string) {
        return string;

    }

    @Override
    public boolean isNotNullOrEmpty(String attribute) {
        return attribute != null && !attribute.equals("") && !attribute.equals(" ");
    }
}
