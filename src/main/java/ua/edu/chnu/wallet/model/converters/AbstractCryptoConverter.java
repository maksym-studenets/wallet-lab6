package ua.edu.chnu.wallet.model.converters;

import lombok.Getter;
import ua.edu.chnu.wallet.model.converters.secure.EncryptionInitializer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.AttributeConverter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static ua.edu.chnu.wallet.model.converters.secure.EncryptionKey.DATABASE_ENCRYPTION_KEY;
import static ua.edu.chnu.wallet.model.converters.secure.EncryptionKey.INIT_VECTOR;

@Getter
public abstract class AbstractCryptoConverter<T> implements AttributeConverter<T, String> {

    protected EncryptionInitializer initializer;

    public AbstractCryptoConverter() {
        this(new EncryptionInitializer());
    }

    public AbstractCryptoConverter(EncryptionInitializer initializer) {
        this.initializer = initializer;
    }

    @Override
    public String convertToDatabaseColumn(T attribute) {
        if ((DATABASE_ENCRYPTION_KEY != null && !DATABASE_ENCRYPTION_KEY.equals("")) &&
                INIT_VECTOR != null && isNotNullOrEmpty(attribute)) {
            try {
                Cipher cipher = initializer.prepareAndInitCipher(Cipher.ENCRYPT_MODE, DATABASE_ENCRYPTION_KEY);
                return encrypt(cipher, attribute);
            } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException |
                    BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException e) {
                throw new RuntimeException(e);
            }
        }
        return attributeToString(attribute);
    }

    @Override
    public T convertToEntityAttribute(String s) {
        assert DATABASE_ENCRYPTION_KEY != null;
        if (s != null && !s.equals("")) {
            try {
                Cipher cipher = initializer.prepareAndInitCipher(Cipher.DECRYPT_MODE, DATABASE_ENCRYPTION_KEY);
                return decrypt(cipher, s);
            } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidAlgorithmParameterException |
                    BadPaddingException | NoSuchPaddingException | IllegalBlockSizeException e) {
                throw new RuntimeException(e);
            }
        }
        return stringToAttribute(s);
    }

    public abstract String attributeToString(T attribute);

    public abstract T stringToAttribute(String string);

    public abstract boolean isNotNullOrEmpty(T attribute);

    public byte[] callCipherDoFinal(Cipher cipher, byte[] bytes) throws IllegalBlockSizeException,
            BadPaddingException {
        return cipher.doFinal(bytes);
    }

    private String encrypt(Cipher cipher, T attribute) throws IllegalBlockSizeException, BadPaddingException {
        byte[] bytesToEncrypt = attributeToString(attribute).getBytes();
        byte[] encryptedBytes = callCipherDoFinal(cipher, bytesToEncrypt);
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    private T decrypt(Cipher cipher, String dbData) throws IllegalBlockSizeException, BadPaddingException {
        byte[] encryptedBytes = Base64.getDecoder().decode(dbData);
        byte[] decryptedBytes = callCipherDoFinal(cipher, encryptedBytes);
        return stringToAttribute(new String(decryptedBytes));
    }
}
