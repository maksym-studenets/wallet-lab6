package ua.edu.chnu.wallet.model.exception;

public class InsufficientFundsException extends RuntimeException {

    public InsufficientFundsException() {
        super("Insufficient funds!");
    }
}
