package ua.edu.chnu.wallet.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import ua.edu.chnu.wallet.model.base.Identifiable;
import ua.edu.chnu.wallet.model.converters.BigDecimalCryptoConverter;
import ua.edu.chnu.wallet.model.converters.StringCryptoConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "transactions")
@EntityListeners(AuditingEntityListener.class)
public class Transaction implements Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "int", nullable = false)
    private Long id;

    @Column(name = "created_at", nullable = false, updatable = false)
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @CreatedDate
    private LocalDateTime createdAt;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Currency currency;

    @Column(name = "balance_before", nullable = false)
    @Convert(converter = BigDecimalCryptoConverter.class)
    private BigDecimal balanceBefore;

    @Column(nullable = false)
    @Convert(converter = BigDecimalCryptoConverter.class)
    private BigDecimal income;

    @Column(nullable = false)
    @Convert(converter = BigDecimalCryptoConverter.class)
    private BigDecimal expense;

    @Column(name = "balance_after", nullable = false)
    @Convert(converter = BigDecimalCryptoConverter.class)
    private BigDecimal balanceAfter;

    @Column
    @Convert(converter = StringCryptoConverter.class)
    private String description;
}
