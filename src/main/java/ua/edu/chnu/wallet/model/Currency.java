package ua.edu.chnu.wallet.model;

public enum Currency {
    USD,
    EUR,
    UAH
}
