package ua.edu.chnu.wallet.model.converters;

import ua.edu.chnu.wallet.model.converters.secure.EncryptionInitializer;

import javax.persistence.Converter;
import java.math.BigDecimal;

@Converter
public class BigDecimalCryptoConverter extends AbstractCryptoConverter<BigDecimal> {

    public BigDecimalCryptoConverter() {
        this(new EncryptionInitializer());
    }

    public BigDecimalCryptoConverter(EncryptionInitializer cipherInitializer) {
        super(cipherInitializer);
    }

    @Override
    public String attributeToString(BigDecimal attribute) {
        return attribute.toString();
    }

    @Override
    public BigDecimal stringToAttribute(String string) {
        return new BigDecimal(string);
    }

    @Override
    public boolean isNotNullOrEmpty(BigDecimal attribute) {
        return attribute != null;
    }
}
