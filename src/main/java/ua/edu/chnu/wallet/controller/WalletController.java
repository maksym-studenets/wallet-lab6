package ua.edu.chnu.wallet.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.edu.chnu.wallet.dto.WalletDepositDto;
import ua.edu.chnu.wallet.service.AuthenticationService;
import ua.edu.chnu.wallet.service.TransactionService;

import javax.validation.Valid;

@RestController
@RequestMapping("/wallets")
@RequiredArgsConstructor
public class WalletController {

    private final AuthenticationService authenticationService;
    private final TransactionService transactionService;

    @PostMapping
    public ResponseEntity depositMoney(@RequestHeader String auth,
                                       @RequestBody @Valid WalletDepositDto walletDepositDto) {
        if (!authenticationService.authenticate(auth)) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        transactionService.depositMoney(walletDepositDto);
        return new ResponseEntity(HttpStatus.OK);
    }
}
