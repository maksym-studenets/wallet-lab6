package ua.edu.chnu.wallet.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.edu.chnu.wallet.dto.ClientDto;
import ua.edu.chnu.wallet.service.AuthenticationService;
import ua.edu.chnu.wallet.service.ClientService;

@RestController
@RequestMapping("/clients")
@RequiredArgsConstructor
public class ClientController {

    private final AuthenticationService authenticationService;
    private final ClientService clientService;

    @GetMapping("/{id}")
    public ResponseEntity<ClientDto> getClientById(@RequestHeader String auth, @PathVariable Long id) {
        return authenticationService.authenticate(auth)
                ? new ResponseEntity<>(clientService.findClientById(id), HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
}
