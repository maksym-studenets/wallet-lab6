package ua.edu.chnu.wallet.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.edu.chnu.wallet.service.AuthenticationService;
import ua.edu.chnu.wallet.service.OrderedFilesService;

@RestController
@RequestMapping("/files")
@RequiredArgsConstructor
public class FileController {

    private final AuthenticationService authenticationService;
    private final OrderedFilesService orderedFilesService;

    @GetMapping("/{fileId}")
    public ResponseEntity downloadFile(@RequestHeader String auth,
                                       @PathVariable Long fileId, @RequestParam Long transactionId) {
        boolean authenticated = authenticationService.authenticate(auth);
        if (!authenticated) {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        return orderedFilesService.checkFileDownloadEligibility(fileId, transactionId)
                ? new ResponseEntity(HttpStatus.OK)
                : new ResponseEntity(HttpStatus.PAYMENT_REQUIRED);
    }
}
