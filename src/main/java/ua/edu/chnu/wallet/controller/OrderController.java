package ua.edu.chnu.wallet.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.edu.chnu.wallet.dto.OrderDto;
import ua.edu.chnu.wallet.dto.TransactionDto;
import ua.edu.chnu.wallet.service.AuthenticationService;
import ua.edu.chnu.wallet.service.OrderService;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {

    private final AuthenticationService authenticationService;
    private final OrderService orderService;

    @PostMapping(value = "/place", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TransactionDto> createOrder(@RequestHeader String auth,
                                                      @RequestBody OrderDto orderDto) {
        if (!authenticationService.authenticate(auth)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        TransactionDto transactionDto = orderService.makeOrder(orderDto);
        return new ResponseEntity<>(transactionDto, HttpStatus.OK);
    }
}
