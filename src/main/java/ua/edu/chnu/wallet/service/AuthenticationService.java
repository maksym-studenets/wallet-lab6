package ua.edu.chnu.wallet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.edu.chnu.wallet.repository.ClientRepository;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final ClientRepository clientRepository;

    public boolean authenticate(final String token) {
        return clientRepository.findFirstByToken(token).isPresent();
    }
}
