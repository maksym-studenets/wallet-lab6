package ua.edu.chnu.wallet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.edu.chnu.wallet.dto.WalletDepositDto;
import ua.edu.chnu.wallet.model.Client;
import ua.edu.chnu.wallet.model.Currency;
import ua.edu.chnu.wallet.model.Transaction;
import ua.edu.chnu.wallet.repository.TransactionRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TransactionService {

    private final TransactionRepository repository;

    @Transactional
    public Transaction createExpenseTransaction(final Transaction previousTransaction, final BigDecimal amount,
                                         final String description) {
        Client client = previousTransaction.getClient();
        BigDecimal balanceBefore = previousTransaction.getBalanceAfter();

        Transaction transaction = new Transaction();
        transaction.setClient(client);
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setCurrency(Currency.UAH);
        transaction.setBalanceBefore(balanceBefore);
        transaction.setIncome(BigDecimal.ZERO);
        transaction.setExpense(amount);
        transaction.setBalanceAfter(balanceBefore.subtract(amount));
        transaction.setDescription(description);
        return repository.save(transaction);
    }

    @Transactional
    public List<Transaction> createDefaultTransactions(List<Transaction> transactions) {
        return repository.saveAll(transactions);
    }

    @Transactional(readOnly = true)
    public Transaction getCurrentClientWalletState(final Long clientId) {
        return repository.findFirstByClientIdOrderByCreatedAtDesc(clientId);
    }

    @Transactional
    public void depositMoney(final WalletDepositDto walletDepositDto) {
        Transaction previousTransaction = repository
                .findFirstByClientIdOrderByCreatedAtDesc(walletDepositDto.getClientId());

        Transaction transaction = new Transaction();
        transaction.setClient(previousTransaction.getClient());
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setCurrency(Currency.UAH);
        transaction.setBalanceBefore(previousTransaction.getBalanceAfter());
        transaction.setIncome(walletDepositDto.getAmount());
        transaction.setExpense(BigDecimal.ZERO);
        transaction.setBalanceAfter(transaction.getBalanceBefore().add(transaction.getIncome()));
        repository.save(transaction);
    }
}
