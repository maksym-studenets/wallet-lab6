package ua.edu.chnu.wallet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.edu.chnu.wallet.model.MyFile;
import ua.edu.chnu.wallet.repository.MyFileRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MyFileService {

    private final MyFileRepository repository;

    @Transactional
    public void createDefaultFiles(List<MyFile> myFiles) {
        repository.saveAll(myFiles);
    }

    @Transactional(readOnly = true)
    public MyFile findById(final Long id) {
        return repository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(1));
    }
}
