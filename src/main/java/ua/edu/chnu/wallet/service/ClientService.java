package ua.edu.chnu.wallet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.edu.chnu.wallet.dto.ClientDto;
import ua.edu.chnu.wallet.model.Client;
import ua.edu.chnu.wallet.repository.ClientRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientService {

    private final ClientRepository repository;
    private final TransactionService transactionService;

    @Transactional
    public List<Client> createDefaultClients(List<Client> clients) {
        return repository.saveAll(clients);
    }

    @Transactional(readOnly = true)
    public ClientDto findClientById(final Long id) {
        Client client = repository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(1));
        ClientDto clientDto = new ClientDto();
        clientDto.setId(client.getId());
        clientDto.setName(client.getName());
        return clientDto;
    }
}
