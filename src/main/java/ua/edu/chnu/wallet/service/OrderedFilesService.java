package ua.edu.chnu.wallet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.edu.chnu.wallet.model.OrderedFiles;
import ua.edu.chnu.wallet.repository.OrderedFilesRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderedFilesService {

    private final OrderedFilesRepository repository;

    @Transactional
    public void createOrderedFiles(List<OrderedFiles> orderedFiles) {
        repository.saveAll(orderedFiles);
    }

    @Transactional(readOnly = true)
    public boolean checkFileDownloadEligibility(final Long fileId, final Long transactionId) {
        return repository.findFirstByTransactionIdAndFileId(transactionId, fileId).isPresent();
    }
}
