package ua.edu.chnu.wallet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.edu.chnu.wallet.dto.OrderDto;
import ua.edu.chnu.wallet.dto.TransactionDto;
import ua.edu.chnu.wallet.model.MyFile;
import ua.edu.chnu.wallet.model.OrderedFiles;
import ua.edu.chnu.wallet.model.Transaction;
import ua.edu.chnu.wallet.model.exception.InsufficientFundsException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final MyFileService myFileService;
    private final OrderedFilesService orderedFilesService;
    private final TransactionService transactionService;

    @Transactional
    public TransactionDto makeOrder(final OrderDto orderDto) {
        Transaction currentWalletState = transactionService.getCurrentClientWalletState(orderDto.getClientId());
        List<MyFile> files = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        BigDecimal orderTotal = BigDecimal.ZERO;

        for (var id : orderDto.getFileIds()) {
            MyFile file = myFileService.findById(id);
            files.add(file);
            orderTotal = orderTotal.add(file.getPrice());
            stringBuilder.append(file.getName()).append("; ");
        }
        String description = stringBuilder.toString();

        if (orderTotal.compareTo(currentWalletState.getBalanceAfter()) > 0) {
            throw new InsufficientFundsException();
        }

        Transaction transaction = transactionService.createExpenseTransaction(currentWalletState, orderTotal, description);

        var orderedFiles = new ArrayList<OrderedFiles>();
        for (var file : files) {
            OrderedFiles orderedFile = new OrderedFiles();
            orderedFile.setTransaction(transaction);
            orderedFile.setFile(file);
            orderedFiles.add(orderedFile);
        }

        orderedFilesService.createOrderedFiles(orderedFiles);

        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setTransactionId(transaction.getId());
        transactionDto.setFileIds(orderedFiles.stream().map(OrderedFiles::getId).collect(Collectors.toList()));
        return transactionDto;
    }

}
