package ua.edu.chnu.wallet.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class OrderDto {

    private Long clientId;
    private List<Long> fileIds;
}
