package ua.edu.chnu.wallet.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TransactionDto {

    private Long transactionId;
    private List<Long> fileIds;
}
