package ua.edu.chnu.wallet.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class WalletDepositDto {

    @NotNull
    private Long clientId;

    @NotNull
    private BigDecimal amount;
}
