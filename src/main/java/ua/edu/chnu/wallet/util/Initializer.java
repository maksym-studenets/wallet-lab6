package ua.edu.chnu.wallet.util;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ua.edu.chnu.wallet.model.Client;
import ua.edu.chnu.wallet.model.Currency;
import ua.edu.chnu.wallet.model.MyFile;
import ua.edu.chnu.wallet.model.Transaction;
import ua.edu.chnu.wallet.service.ClientService;
import ua.edu.chnu.wallet.service.MyFileService;
import ua.edu.chnu.wallet.service.TransactionService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class Initializer implements ApplicationListener<ApplicationReadyEvent> {

    private final ClientService clientService;
    private final MyFileService myFileService;
    private final TransactionService transactionService;

    @Override
    @Transactional
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        /*var clients = clientService.createDefaultClients(getDefaultClients());
        transactionService.createDefaultTransactions(getDefaultTransactions(clients));
        myFileService.createDefaultFiles(getDefaultFiles());*/
    }

    private List<Client> getDefaultClients() {
        var clients = new ArrayList<Client>();

        var client = new Client();
        client.setName("Miguel Hernandez");
        client.setToken(UUID.randomUUID().toString().replace("-", ""));
        clients.add(client);

        client = new Client();
        client.setName("Fernando Gonzalez");
        client.setToken(UUID.randomUUID().toString().replace("-", ""));
        clients.add(client);

        client = new Client();
        client.setName("Jose Garcia");
        client.setToken(UUID.randomUUID().toString().replace("-", ""));
        clients.add(client);

        return clients;
    }

    private List<Transaction> getDefaultTransactions(List<Client> clients) {

        clients.sort(Comparator.comparing(Client::getId));

        var transactions = new ArrayList<Transaction>();

        Transaction transaction = new Transaction();
        transaction.setClient(clients.get(0));
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setCurrency(Currency.UAH);
        transaction.setBalanceBefore(BigDecimal.ZERO);
        transaction.setIncome(new BigDecimal("20"));
        transaction.setExpense(BigDecimal.ZERO);
        transaction.setBalanceAfter(transaction.getBalanceBefore().add(transaction.getIncome())
                .subtract(transaction.getExpense()));
        transactions.add(transaction);

        transaction = new Transaction();
        transaction.setClient(clients.get(1));
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setCurrency(Currency.UAH);
        transaction.setBalanceBefore(BigDecimal.ZERO);
        transaction.setIncome(new BigDecimal("50"));
        transaction.setExpense(BigDecimal.ZERO);
        transaction.setBalanceAfter(transaction.getBalanceBefore().add(transaction.getIncome())
                .subtract(transaction.getExpense()));
        transactions.add(transaction);

        transaction = new Transaction();
        transaction.setClient(clients.get(2));
        transaction.setCreatedAt(LocalDateTime.now());
        transaction.setCurrency(Currency.UAH);
        transaction.setBalanceBefore(BigDecimal.ZERO);
        transaction.setIncome(new BigDecimal("100"));
        transaction.setExpense(BigDecimal.ZERO);
        transaction.setBalanceAfter(transaction.getBalanceBefore().add(transaction.getIncome())
                .subtract(transaction.getExpense()));
        transactions.add(transaction);

        return transactions;
    }

    private List<MyFile> getDefaultFiles() {
        List<MyFile> myFiles = new ArrayList<>();

        MyFile myFile = new MyFile();
        myFile.setName("Lab2.pdf");
        myFile.setPrice(new BigDecimal("15"));
        myFiles.add(myFile);

        myFile = new MyFile();
        myFile.setName("song.mp3");
        myFile.setPrice(new BigDecimal("49.75"));
        myFiles.add(myFile);

        myFile = new MyFile();
        myFile.setName("test.odt");
        myFile.setPrice(new BigDecimal("9.00"));
        myFiles.add(myFile);

        return myFiles;
    }
}
