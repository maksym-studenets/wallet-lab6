package ua.edu.chnu.wallet.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({
        PropertyConfig.EncryptionProperties.class
})
public class PropertyConfig {

    @Getter
    @Setter
    @ConfigurationProperties("encryption")
    public static class EncryptionProperties {
        private String key;
        private String initVector;
    }
}
